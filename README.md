# My Message Information JSON Solution

This project contains a solution to an interview problem I was given.

I wrote my code in a virtualenv using Python 3.4.0 on Ubuntu 14.04.  I have
included a requirements.txt file that is a pip freeze of all the distributions
I used for development, testing, and end product.

I used lxml, http://lxml.de/, for parsing html and extracting the title.
Installing lxml using pip may not work on a clean ubuntu 14.04 machine.
Following the instructions on the lxml site you may need to run
`sudo apt-get install libxml2-dev libxslt-dev python-dev`

Follow the following steps for running the unittests:

```
$ cd message_infromation
$ virtualenv -p <your path to python3> VENV
$ source VENV/bin/activate
$ pip install -r requirements.txt
$ nosetests
```

The regex pattern I used for matching links is open source code from,
https://gist.github.com/dperini/729294. I have kept the license with it
and placed it in its own module, link_pattern.

# Original Problem Statement

Please write, in your preferred language, code that takes a chat message string
and returns a JSON string containing information about its contents. Special co
ntent to look for includes:

1. @mentions - A way to mention a user. Always starts with an '@' and ends when
hitting a non-word character. (http://help.hipchat.com/knowledgebase/articles/6
4429-how-do-mentions-work-)

2. Emoticons - For this exercise, you only need to consider 'custom' emoticons
which are ASCII strings, no longer than 15 characters, contained in parenthesis
. You can assume that anything matching this format is an emoticon. (http://hip
chat-emoticons.nyh.name)

3. Links - Any URLs contained in the message, along with the page's title.

For example, calling your function with the following inputs should result in t
he corresponding return values.
Input: "@chris you around?"
Return (string):
{
  "mentions": [
    "chris"
  ]
}


Input: "Good morning! (megusta) (coffee)"
Return (string):
{
  "emoticons": [
    "megusta",
    "coffee"
  ]
}


Input: "Olympics are starting soon; http://www.nbcolympics.com"
Return (string):
{
  "links": [
    {
      "url": "http://www.nbcolympics.com",
      "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
    }
  ]
}


Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
Return (string):
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ]
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
    }
  ]
}


There is no enforced time limit on this exercise.  Work on it until you're sati
sfied and feel it represents the quality of work you care to show off. Please t
ake your time to make sure it is quality code. If this goes well the final step
is an onsite interview!

Please create your project as a git repository that can be publicly viewed eith
er on bitbucket.org or github.com and send the link to your repository for us t
o review.