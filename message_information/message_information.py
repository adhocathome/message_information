"""
This module is used for extracting information from messages.

The information is provided in json format and contains all
emoticons, links, and mentions from a message.
"""

import json
import logging
import re
import urllib.parse
import urllib.robotparser

import requests
from lxml import html

from .link_pattern import LINK_PATTERN


LOG = logging.getLogger(__name__)
MENTION_PATTERN = re.compile(r"(?:^|\s)@(\w+)")
EMOTICON_PATTERN = re.compile(r'\((\w{1,15})\)', re.ASCII)


def to_json(message):
    """Abstracts information from the provided message.

    The message provided will be analyzed and all mentions, emoticons, and
    links. The message information will be returned as json.  An example
    message and the information returned:

    message = '@bob (megusta) (coffee) from http://www.dailyjo.com'
    json = '{
        "emoticons":[
            "megusta",
            "coffee"
        ],
        "links":[
            {
                "title":"Your Daily Cup of Jo",
                "url":"http://www.dailyjo.com"
            }
        ],
        "mentions":[
            "bob"
        ]
    }'

    Args:
      message (str): The message that will be analyzed.

    Returns:
      string: A json formatted string like the above axample

    """
    info = dict()

    def _set_info(key, new_info):
        """Helper for adding non-empty list to dict."""
        if len(new_info) > 0:
            info[key] = new_info

    LOG.debug('Converting message to json %s', message)
    _set_info('mentions', findall(MENTION_PATTERN, message))
    _set_info('emoticons', findall(EMOTICON_PATTERN, message))
    link_info = [{'url': l, 'title': get_title(l)} for l in findall(LINK_PATTERN, message)]
    _set_info('links', link_info)

    return json.dumps(info)


def can_fetch(url, agent='*'):
    """Determines if the provided url can be fetched by us.

    This function will parse the url and check the netloc for a robots.txt
    resource. If the robots.txt resource is found then it will be checked
    to see if our user agent, default *, will be allowed to fetch the url. If the
    robots.txt resource does not exist it will be assumed we are allowed
    to fetch.

    Note:
      The context in which this function is used allows us to assume
      that urls will include the scheme, like http.

    Args:
      url (str): The url that we want to test for fetch-ability.
      agent (str, optional): The user agent fetching the url.

    Returns:
      bool: True if we can fetch, False otherwise.
    """
    split_url = urllib.parse.urlsplit(url)
    if not split_url.scheme.startswith('http'):
        return True

    root = '{}://{}'.format(split_url.scheme, split_url.netloc)
    robots_url = urllib.parse.urljoin(root, 'robots.txt')
    LOG.debug('Checking permission to scrape with %s', robots_url)

    rbt_parser = urllib.robotparser.RobotFileParser()
    rbt_parser.set_url(robots_url)

    allowed = rbt_parser.can_fetch(agent, url)
    if allowed:
        LOG.debug('Allowed to scrape %s', url)
        return True
    else:
        LOG.debug('NOT allowed to scrape %s', url)
        return False


    return rbt_parser.can_fetch(agent, url)


def get_title(url, timeout=2, check_robot=True):
    """Provides the title from the html of the provided url.

    Scraps the provided url's html for the title attribute
    and provides the title if it exists. This function makes a
    best effort attempt.  This means that if the page can
    not be scraped, the title does not exist, or the resource
    is not html then None will be returned instead of the title.

    Args:
      url (str): The url to acquire the title of.
      timeout (int, optional): The number of seconds to wait for a response.
        Default is 2 seconds.
      check_robot (bool, optional): Whether or not to check the robots.txt
        before scraping a web page for the title. Default is True.

    Returns:
      str: The title of the web page at provided url or None
    """
    if check_robot:
        if not can_fetch(url):
            LOG.info('Robot not allowed to scan %s', url)
            return None
    else:
        LOG.warning('Scraping the web without consulting robots.txt.')

    try:
        response = requests.get(url, timeout=timeout)
        if response.status_code != 200:
            LOG.error('Did not receive a 200 response.')
            return None

        LOG.debug('Scraping %s for the title element.', url)
        html_tree = html.fromstring(response.text)
        title = html_tree.xpath('//title/text()')

        if title:
            return title[0]
    except requests.exceptions.RequestException as err:
        LOG.exception(err)
        return None


def findall(pattern, message):
    """Applies the pattern to the message and provides all matches.

    Args:
      pattern (re object): Regex pattern with findall method.
      message (string): The message that will be scanned.

    Returns:
      list: A list of all the regex matches in the message.
    """
    matches = pattern.findall(message)
    LOG.debug('Matches in message: %s', matches)
    return matches
