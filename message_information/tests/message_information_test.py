import unittest
import json
from unittest.mock import MagicMock, patch

import requests

from message_information import message_information


class TestMessageInformation(unittest.TestCase):

    """
    Unittests for the message_information module.
    """

    def test_example_mention(self):
        """Simple example message with a mention."""
        message = '@chris you around?'
        desired_json = json.dumps(
            {
                'mentions': [
                    'chris'
                ]
            }
        )
        self._test_example_message(message, desired_json)

    def test_example_emoticons(self):
        """Simple example message with emoticons."""
        message = 'Good morning! (megusta) (coffee)'
        desired_json = json.dumps(
            {
                'emoticons': [
                    'megusta',
                    'coffee'
                ]
            }
        )
        self._test_example_message(message, desired_json)

    @patch('message_information.message_information.can_fetch')
    @patch('requests.get')
    def test_example_link(self, mock_get, mock_can_fetch):
        """Simple example message with a link."""
        test_title = 'SXSW | 2015'
        mock_get.return_value = MagicMock(
            text='<html><head><title>{}</title></head>'.format(test_title),
            status_code=200
        )
        mock_can_fetch.return_value = True

        url = 'http://www.sxsw.com'
        message = 'SXSW is starting soon; {}'.format(url)
        desired_json = json.dumps(
            {
                'links' : [
                    {
                        'url': 'http://www.sxsw.com',
                        'title': test_title
                    }
                ]
            }
        )

        self._test_example_message(message, desired_json)

    @patch('message_information.message_information.can_fetch')
    @patch('requests.get')
    def test_combo_message(self, mock_get, mock_can_fetch):
        """Message with links, mentions, and emoticons."""
        mock_can_fetch.return_value = True
        test_title = 'twitter | jdorfman'
        mock_get.return_value = MagicMock(
            text='<html><head><title>{}</title></head>'.format(test_title),
            status_code=200
        )
        message = ('@bob @john (success) such a cool feature;'
                   'https://twitter.com/jdorfman/status/432134')
        desired_json = json.dumps(
            {
                'mentions': [
                    'bob',
                    'john'
                ],
                'emoticons': [
                    'success'
                ],
                'links' : [
                    {
                        'url': 'https://twitter.com/jdorfman/status/432134',
                        'title': 'twitter | jdorfman'
                    }
                ]
            }
        )
        self._test_example_message(message, desired_json)

    def test_email_address_not_mention(self):
        """Email addresses don't show up as mentions."""

        message = '@jdm his email is bryan@foo.com'
        desired_json = json.dumps(
            {
                'mentions': [
                    'jdm'
                ]
            }
        )
        self._test_example_message(message, desired_json)

    def test_long_emoticons(self):
        """Emoticons are less than or equal to 15 characters."""
        max_emote = 'x' * 15
        too_big_emote = 'y' * 16
        message = 'Hello({}) -- Goodbye({})'.format(max_emote, too_big_emote)
        desired_json = json.dumps(
            {
                'emoticons': [
                    max_emote
                ]
            }
        )
        self._test_example_message(message, desired_json)

    @patch('message_information.message_information.can_fetch')
    @patch('requests.get')
    def test_html_without_title(self, mock_get, mock_can_fetch):
        """HTML web page has no title."""
        mock_can_fetch.return_value = True
        mock_get.return_value = MagicMock(
            text='<html><head></head></html>',
            status_code=200
        )
        url = 'http://test-page.com'
        message = 'Please visit {}'.format(url)
        desired_json = json.dumps(
            {
                'links' : [
                    {
                        'url': url,
                        'title': None
                    }
                ]
            }
        )
        self._test_example_message(message, desired_json)

    @patch('message_information.message_information.can_fetch')
    @patch('requests.get')
    def test_not_ok_response(self, mock_get, mock_can_fetch):
        """HTTP response is not 200 when scraping."""
        mock_can_fetch.return_value = True
        mock_get.return_value = MagicMock(
            status_code=404
        )

        url = 'http://test-page.com'
        message = 'Please visit {}'.format(url)
        desired_json = json.dumps(
            {
                'links' : [
                    {
                        'url': url,
                        'title': None
                    }
                ]
            }
        )
        self._test_example_message(message, desired_json)

    @patch('message_information.message_information.can_fetch')
    @patch('requests.get')
    def test_cannot_fetch(self, mock_get, mock_can_fetch):
        """Blocked by robots.txt"""
        mock_can_fetch.return_value = False
        url = 'http://test-page.com'
        message = 'Please visit {}'.format(url)
        desired_json = json.dumps(
            {
                'links' : [
                    {
                        'url': url,
                        'title': None
                    }
                ]
            }
        )
        self._test_example_message(message, desired_json)

    @patch('message_information.message_information.can_fetch')
    @patch('requests.get')
    def test_requests_exception(self, mock_get, mock_can_fetch):
        """Exception requesting webpage."""
        mock_can_fetch.return_value = True
        mock_get.side_effect = requests.exceptions.RequestException
        url = 'http://test-page.com'
        message = 'Please visit {}'.format(url)
        desired_json = json.dumps(
            {
                'links' : [
                    {
                        'url': url,
                        'title': None
                    }
                ]
            }
        )
        self._test_example_message(message, desired_json)

    @patch('urllib.robotparser.RobotFileParser')
    def test_can_fetch(self, mock_RobotFileParser):
        """Robot parser is used correctly."""
        url = 'http://test-page.com/some/web/resource'
        message_information.can_fetch(url)
        robot_url = 'http://test-page.com/robots.txt'
        mock_RobotFileParser().set_url.assert_called_once_with(robot_url)
        mock_RobotFileParser().can_fetch.assert_called_once_with('*', url)

    def _test_example_message(self, message, desired_info):
        """Helper for asserting return message info json."""
        actual_info = message_information.to_json(message)
        self.assertEquals(desired_info, actual_info)
