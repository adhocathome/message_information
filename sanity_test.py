#!/usr/bin/env python

import logging
import sys

from message_information.message_information import to_json


def main():
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    # Sanity and functional tests.
    print(to_json(u'@jdm This is hello@jdm. I @jdm am on my way to see @chris.'))
    print(to_json(u'Please visit the new (smile) cafe at http://www.google.com'))
    print(to_json(u'This is a new link, http://www.nbcolympics.com, and this is'
                  u'another https://www.google.com'))


if __name__ == '__main__':
    main()
